# Gitee::Frontend

UI component library for Gitee.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitee-frontend-rails'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gitee-frontend

## Usage

Append the following line to **app/assets/javascripts/application.js**:

``` js
//= require gitee-frontend
```

Append the following line to **app/assets/stylesheets/application.css**:

``` css
/*= require gitee-frontend */
```

Demo: [https://mayun-team.gitee.io/gitee-frontend/ ]( https://mayun-team.gitee.io/gitee-frontend/ )

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).
